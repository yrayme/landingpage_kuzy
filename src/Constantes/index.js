import { createMuiTheme, } from '@material-ui/core/styles';
import cuero from '../Components/img/cuero.svg';
import shoes from '../Components/img/shoes.svg';
import packages from '../Components/img/packages.svg';
import consult from '../Components/img/counseling.svg';
import moda1 from '../Components/img/moda1.jpg';
import moda2 from '../Components/img/moda2.jpg';
import moda3 from '../Components/img/moda3.jpg';
import moda4 from '../Components/img/moda4.jpg';


export const KUZY_THEME = createMuiTheme({
  typography: {
    fontFamily: 'Gotham',
    fontStyle: 'normal',
    fontWeight: 'normal',
    src: 'url(../gothamotf)',
  },
  palette: {
    primary: {
      main: '#E1153F',
    },
    secondary: {
      main: '#300862',
    }
  },
  overrides: {
    MuiMobileStepper: {
      dot: {
        backgroundColor: 'white',
        width: 10,
        height: 10,
        border: '0.5px solid #E1153F',
      }
    },

    MuiBadge: {
      badge: {
        width: 30,
        height: 30,
        borderRadius: '50%',
        fontSize: '15px',
        marginLeft: 10
      }
    },

    MuiContainer: {
      maxWidthXl: {
        maxWidth: '100%',
      }
    },
  }
});

export const VARIETY = [
  {
    title: 'Cuero trujillano',
    img: cuero,
    desc: 'Corta descripción del beneficio',
  },
  {
    title: 'Variedad',
    img: shoes,
    desc: 'Corta descripción del beneficio',
  },
  {
    title: 'Stock',
    img: packages,
    desc: 'Corta descripción del beneficio',
  },
  {
    title: 'Consultas online',
    img: consult,
    desc: 'Corta descripción del beneficio'
  }
]

export const VARIETY2 = [
  {
    img: moda1,
    col: 1,
    row: 1,
    marginTop: '0px',
    height: '218px',
  },
  {
    img: moda2,
    col: 1,
    row: 1.5,
    marginTop: '0px',
    height: '318px',
  },
  {
    img: moda3,
    col: 1,
    row: 2,
    marginTop: '0px',
    height: '418px',
  },
  {
    img: moda3,
    col: 1,
    row: 2,
    marginTop: '-200px',
    height: '418px',
  },
  {
    img: moda2,
    col: 1,
    row: 1.5,
    marginTop: '-100px',
    height: '318px',
  },
  {
    img: moda1,
    col: 1,
    row: 1,
    marginTop: '0px',
    height: '218px',
  },
  {
    img: moda1,
    col: 2,
    row: 1,
    marginTop: '0px',
    height: '218px',
  },
  {
    img: moda2,
    col: 1,
    row: 1,
    marginTop: '0px',
    height: '218px',
  },
]

export const tutorialSteps = [
  {
    img: moda1,
    img1: moda2,
    img2: moda3,
  },
  {
    img: moda3,
    img1: moda2,
    img2: moda1,
  },
  {
    img: moda4,
    img1: moda3,
    img2: moda2,
  },
  {
    img: moda2,
    img1: moda3,
    img2: moda1,
  },
]

