/*Dependencias*/
import React, { useState } from 'react';
import { Typography, Container, Grid, Button, Badge, MobileStepper, Link } from '@material-ui/core';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Letra from '../img/logo1.png';
import tacon from '../img/tacon.svg';
import caja from '../img/caja.svg';
import phone from '../img/phone.svg';
import flecha from '../img/flecha.svg';
import face from '../img/facebook.svg';
import instagram from '../img/instagram.svg';
import Whatsaap from '../img/Whatsaap.svg';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';

import { VARIETY, VARIETY2, tutorialSteps } from '../../Constantes';
import './../index.css';

const AutoPlaySwipeableViews = autoPlay(SwipeableViews);


/*Estilos para el collage de imagenes */
const list = makeStyles((theme) => ({
    img: {
        height: 'auto',
        display: 'block',
        maxWidth: 3000,
        overflow: 'hidden',
        width: '100%',
        marginBottom: '20px'
    },

    [theme.breakpoints.down('xs')]: {
        root: {
            display: 'none'
        },
    },
}));

const list1 = makeStyles((theme) => ({
    img: {
        maxHeight: 255,
        display: 'block',
        maxWidth: 600,
        overflow: 'hidden',
        width: '100%',
        marginBottom: '20px'
    },
    [theme.breakpoints.up('md')]: {
        root: {
            display: 'none'
        },
    },
    [theme.breakpoints.up('lg')]: {
        root: {
            display: 'none'
        },
    },
    [theme.breakpoints.up('sm')]: {
        root: {
            display: 'none'
        },
    }   


}));





const Body = () => {

    const classes = list();
    const classes1 = list1();
    const theme = useTheme();
    const maxSteps = tutorialSteps.length;

    const [activeStep, setActiveStep] = useState(0);

    const handleStepChange = (step) => {
        setActiveStep(step);
    };

    return (
        <Grid >
            <Container style={{ marginTop: -100, marginBottom: 20 }} className='body__mobile' maxWidth="xl">
                <Grid className='body__spacing__mobile'>
                    <Typography class='body__titulo'>Porqué elegirnos</Typography>
                    <div className='body__puntoTitulo'></div>
                    <hr className='body__hrTitulo' />
                    <Grid container spacing={5}>
                        {VARIETY.map(vari => (
                            <Grid xs={6} sm={3} md={3} align='center' className='body__var__mobile'>
                                <div className='body__circle'>
                                    <img src={vari.img} alt='' className='body__img' />
                                </div>
                                <Typography class='body__title'>{vari.title}</Typography>
                                <div className='body__punto'></div>
                                <hr className='body__hr' />
                                <Typography class='body__desc'>{vari.desc}</Typography>
                            </Grid>
                        ))}
                    </Grid>
                </Grid>
            </Container>
            <Grid>
                <Container style={{ marginTop: 50 }} className='body__mobile' maxWidth="xl">
                    <Grid className='body__spacing__model'>
                        <Typography class='body__titulo'>Nuestros modelos</Typography>
                        <div className='body__puntoTitulo'></div>
                        <hr className='body__hrTitulo' />
                        <GridList cellHeight={200} spacing={18} className={classes.root} cols={3}>
                            {VARIETY2.map((tile) => (
                                <GridListTile key={tile.img} cols={tile.col} rows={tile.row} style={{ marginTop: tile.marginTop }}>
                                    <img src={tile.img} alt={tile.title}  style={{maxHeight: tile.height}}/>
                                    <GridListTileBar
                                        style={{ background: 'none' }}
                                        actionIcon={
                                            <IconButton >
                                                <FavoriteBorderIcon style={{ color: 'white' }} />
                                            </IconButton>
                                        }
                                    />
                                </GridListTile>
                            ))}

                        </GridList>
                        <Grid className={classes1.root}>
                            <AutoPlaySwipeableViews
                                axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                index={activeStep}
                                onChangeIndex={handleStepChange}
                                enableMouseEvents
                            >
                                {tutorialSteps.map((step) => (
                                    <>
                                        <img src={step.img} alt='' className={classes1.img} />
                                        <img src={step.img1} alt='' className={classes1.img} />
                                        <img src={step.img2} alt='' className={classes1.img} />
                                    </>
                                ))}
                            </AutoPlaySwipeableViews>
                            <MobileStepper
                                steps={maxSteps}
                                position="static"
                                variant="dots"
                                activeStep={activeStep}
                                style={{ background: 'white', width: 40, marginLeft: '40%', marginTop: -15 }}
                            />

                        </Grid>
                        <Grid align='center'>
                            <div className='body__button'>
                                <Button variant="contained" color="secondary">
                                    <Typography style={{
                                        fontSize: 14, padding: 6,
                                        fontFamily: 'Gotham'
                                    }}>MIRA AQUÍ TODOS NUESTRO MODELOS</Typography>

                                </Button>
                                <hr className='body__hrButton' />

                            </div>
                        </Grid>
                    </Grid>
                </Container>
            </Grid>
            <div className='body__letra' >
                <img src={Letra} alt='' className='body__img__letra' />
            </div>
            <div className='body__model'>
            </div >
            <Container className='body__mobile1' maxWidth='xl'>
                <Grid className='body__spacing__mobile'>
                    <Typography class='body__titulo'>¿Comó hacer mi pedido?</Typography>
                    <div className='body__puntoTitulo'></div>
                    <hr className='body__hrTitulo2' />

                    <Grid container spacing={5} style={{ marginTop: 20 }}>
                        <Grid xs={12} sm={3} md={3} align='center' className='body__spacing'>
                            <Badge color="secondary"
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                badgeContent={1} >
                                <div className='body__circle' style={{ border: '3px solid #48128C', marginTop: -25 }}>
                                    <img src={tacon} alt='' className='body__img' />
                                </div>
                            </Badge>
                            <Typography class='body__title'>Elegir modelo</Typography>
                            <div className='body__punto'></div>
                            <hr className='body__hr' />
                            <Typography class='body__desc'>Corta descripción del beneficio</Typography>
                        </Grid>
                        <Grid xs={12} sm={2} md={2} align='center' className='body__spacing2'>
                            <div >
                                <img src={flecha} alt='' className='body__rotar' />
                            </div>
                        </Grid>
                        <Grid xs={12} sm={3} md={3} align='center' className='body__spacing'>
                            <Badge color="secondary"
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                badgeContent={2} >
                                <div className='body__circle' style={{ border: '3px solid #48128C', marginTop: -25 }}>
                                    <img src={phone} alt='' className='body__img' />
                                </div>
                            </Badge>
                            <Typography class='body__title'>Contactar</Typography>
                            <div className='body__punto'></div>
                            <hr className='body__hr' />
                            <Typography class='body__desc'>Corta descripción del beneficio</Typography>
                        </Grid>
                        <Grid xs={12} sm={2} md={2} align='center' className='body__spacing2' >
                            <div >
                                <img src={flecha} alt='' className='body__rotar' />
                            </div>
                        </Grid>
                        <Grid xs={12} sm={2} md={2} align='center' className='body__spacing'>
                            <Badge color="secondary"
                                anchorOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                }}
                                badgeContent={2} >
                                <div className='body__circle' style={{ border: '3px solid #48128C', marginTop: -25 }}>
                                    <img src={caja} alt='' className='body__img' />
                                </div>
                            </Badge>
                            <Typography class='body__title'>Recoger pedido</Typography>
                            <div className='body__punto'></div>
                            <hr className='body__hr' />
                            <Typography class='body__desc'>Corta descripción del beneficio</Typography>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>

            <Container maxWidth='xl'>
                <Grid className='body__spacing__model'>
                    <Typography class='body__titulo'>Aprovecha nuestras promociones</Typography>
                    <div className='body__puntoTitulo'></div>
                    <hr className='body__hrTitulo3' />
                    <Grid container spacing={1}>
                        <Grid xs={12} sm={6} md={6} align='center'>
                            <div className='body_prom'>
                                <Typography class='promo__title'>Sorteo de una canasta navideña</Typography>
                                <Typography class='promo__subtitle'>Colocar banner de redes sociales</Typography>
                            </div>
                        </Grid>
                        <Grid xs={12} sm={6} md={6} align='center' >
                            <div className='body_prom'>
                                <Typography class='promo__title'>remate 2x1</Typography>
                                <Typography class='promo__subtitle'>Colocar banner de redes sociales</Typography>
                            </div>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>

            <Container maxWidth='lg'>
                <Grid className='body__spacing__redes'>
                    <Grid container spacing={1} >
                        <Grid xs={12} sm={6} md={6} spacing={1} >
                            <Typography class='body__titulo'>Sé parte de nuestra comunidad</Typography>
                            <div className='body__puntoTitulo' ></div>
                            <hr className='body__hrTitulo4' />
                        </Grid>
                        <Grid xs={4} sm={2} md={2} spacing={1} align='center'>
                            <Link href="#" variant="body2">
                                <img src={instagram} alt='' className='body__redes__img' />
                            </Link>
                        </Grid>
                        <Grid xs={4} sm={2} md={2} spacing={1} align='center'>
                            <Link href="#" variant="body2">
                                <img src={Whatsaap} alt='' className='body__redes__img' />
                            </Link>
                        </Grid>
                        <Grid xs={4} sm={2} md={2} spacing={1} align='center'>
                            <Link href="#" variant="body2">
                                <img src={face} alt='' className='body__redes__img' />
                            </Link>
                        </Grid>
                    </Grid>
                </Grid>
            </Container>
            <div className='body__model1'>
            </div >
            <div className={classes1.root}>
                <div className='body__model2' >
                </div >
                <div className='body__model3' >
                </div >
            </div>
        </Grid>



    )

}

export default Body;

