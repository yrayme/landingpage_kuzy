/*Dependencias*/
import React from 'react';
import { Typography, Grid } from '@material-ui/core';
import './../index.css';


const Footer = () => {

    return (
        <Grid className='footer'>
            <hr className='footer__hr' />
            <Typography class='footer__copy' >© Copyrigth 2020, Todos los derechos reservados |<span style={{ color: '#E1153F', fontWeight: 'bold' }}>Kuzy</span> </Typography>
        </Grid>
    )

}

export default Footer;

