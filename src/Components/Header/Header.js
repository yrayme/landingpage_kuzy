/*Dependencias*/
import React from 'react';
import { Grid, Typography, Button, MobileStepper, IconButton, Link } from '@material-ui/core';
import { useTheme } from '@material-ui/core/styles';
import NavigateBefore from '@material-ui/icons/NavigateBefore';
import NavigateNext from '@material-ui/icons/NavigateNext';
import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
import K from '../img/vectorLogo.svg';
import Logo from '../img/Logo.svg';
import ws from '../img/ws.svg';
import moda1 from '../img/moda1.jpg';
import moda2 from '../img/moda2.jpg';
import moda3 from '../img/moda3.jpg';
import moda4 from '../img/moda4.jpg';
import './../index.css';


const AutoPlaySwipeableViews = autoPlay(SwipeableViews);


const tutorialSteps = [
    moda1,
    moda2,
    moda3,
    moda4
];


const Header = () => {
    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);
    const maxSteps = tutorialSteps.length;

    const handleNext = () => {
        setActiveStep((prevActiveStep) => prevActiveStep + 1);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => prevActiveStep - 1);
    };

    const handleStepChange = (step) => {
        setActiveStep(step);
    };

    return (
        <Grid>
            <Grid container className='header'>
                {/* <Grid item xs={2} sm={2} md={2}> */}
                <img src={K} alt='' className='header__vector' />
                {/* </Grid> */}

                <Grid class='header__table' xs={12} sm={12}>
                    <img src={Logo} alt='' className='header__logo' />
                    <div className='header__punto'>
                        <hr className='header__hr' />
                    </div>
                    <Typography class='header__lema'>Alegría en cada paso</Typography>
                    <div className='header__button'>
                        <Button variant="contained" color="primary">
                            <Typography style={{
                                fontSize: 12, padding: 6,
                                fontFamily: 'Gotham'
                            }}>QUIERO VER TODO</Typography>
                        </Button>
                        <hr className='header__hrButton' />
                    </div>
                </Grid>

            </Grid>

            <Grid className='header__carusel'>
                <AutoPlaySwipeableViews
                    axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                    index={activeStep}
                    onChangeIndex={handleStepChange}
                    enableMouseEvents
                >
                    {tutorialSteps.map((step) => (
                        <img src={step} alt='' className='header__carusel__img' />
                    ))}
                </AutoPlaySwipeableViews>
                <MobileStepper
                    steps={maxSteps}
                    position="static"
                    variant="dots"
                    activeStep={activeStep}
                    style={{ background: 'white', width: 40, marginLeft: '40%' }}
                />
                <Grid>
                    <IconButton
                        disabled={activeStep === 0}
                        style={{ color: 'white' }}
                        className='header__back'
                        onClick={handleBack}
                    >
                        <NavigateBefore fontSize="large" />
                    </IconButton>

                </Grid>
                <Grid >
                    <IconButton
                        disabled={activeStep === maxSteps - 1}
                        style={{ color: 'white' }}
                        className='header__next'
                        onClick={handleNext}
                    >
                        <NavigateNext fontSize="large" />
                    </IconButton>

                </Grid>
            </Grid>
            <Grid class='header__Ws'>
            <Link href="https://www.beez.pe/" variant="body2">
  
                <img src={ws} alt='' style={{ marginTop: 9, marginLeft: 7 }} />
                <Typography className='header__p'>959273096</Typography>
                </Link>
                <hr className='header__hrWs' />
            </Grid>


        </Grid>
    )
}

export default Header;

