import Header from './Components/Header/Header';
import Footer from './Components/Footer/Footer';
import Body from './Components/Body/Body';
import { ThemeProvider } from '@material-ui/styles';

/*Constantes */
import { KUZY_THEME } from './Constantes'

import './App.css';


const theme = KUZY_THEME;



function App() {
  return (
    <ThemeProvider theme={theme} >
      <Header />
      <Body/>      
      <Footer />
    </ThemeProvider>
  );
}

export default App;
